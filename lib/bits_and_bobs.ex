defmodule BitsAndBobs do
  use Application

  @moduledoc """
  Documentation for BitsAndBobs.
  """

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link(
      [
        supervisor(ConCache, [[], [name: :my_cache]])
      ],
      strategy: :one_for_one
    )
  end
end
