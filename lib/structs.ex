defmodule Structs do
  alias __MODULE__
  @enforce_keys [:status]

  defstruct(time: nil, status: :init)

  def new(status) do
    %Structs{status: status}
  end
end
