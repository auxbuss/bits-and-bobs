defmodule Euclid do
  @doc """
    Solves Diophantine equations, which have integer solutions for ax + by = c
  """
  def solve({a, b, c}, {xmin, xmax}, {ymin, ymax}) do
    cond do
      not integral_solution?(a, b, c) ->
        :no_solutions

      true ->
        for x <- xmin..xmax,
            y <- ymin..ymax,
            a * x + b * y == c,
            do: {x, y}
    end
  end

  defp integral_solution?(a, b, c) do
    rem(c, gcd(a, b)) == 0
  end

  defp gcd(a, 0), do: a

  defp gcd(a, b) do
    gcd(b, mod(a, b))
  end

  defp mod(a, b) do
    a - b * div(a, b)
  end
end
