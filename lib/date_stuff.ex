defmodule DateStuff do
  def date_match(
        <<day::bytes-size(2)>> <>
          "/" <> <<month::bytes-size(2)>> <> "/" <> <<year::bytes-size(4)>>
      ) do
    try do
      [String.to_integer(day), String.to_integer(month), String.to_integer(year)]
    rescue
      ArgumentError -> raise InvalidDateFormat
    end
  end

  def date_match(_string) do
    raise InvalidDateFormat
  end
end
