defmodule ProcessStorage do
  def start(initial) do
    process =
      spawn_link(fn ->
        execute(initial)
      end)

    Process.register(process, :storage)
    process
  end

  def execute(state) do
    receive do
      {:get, sender} ->
        send(sender, state)
        execute(state)

      {:put, value} ->
        execute(state ++ [value])
    end
  end

  def get do
    send(:storage, {:get, self()})

    receive do
      msg -> msg
    end
  end

  def put(value) do
    send(:storage, {:put, value})
  end
end
