defmodule Metaprog do
  @opts %{time: :am}

  defmacro one_plus_two do
    {:+, [], [1, 2]}
  end

  defmacro say_hi do
    quote do
      IO.puts("hello world")
    end
  end

  defmacro opts do
    Macro.escape(@opts)
  end

  defmacro say_hi_again(name) do
    # This could also be done like this:
    # quote do
    #   "hello #{unquote name}"
    quote bind_quoted: binding() do
      "hello #{name}"
    end
  end
end
