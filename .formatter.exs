[
  inputs: [
    "mix.exs",
    "lib/**/*.{ex,exs}",
    "test/**/*.{ex,exs}"
  ],

  locals_without_parens: []
]
