defmodule TaskTest do
  use ExUnit.Case

  defmodule API do
    def random(sleep \\ true) do
      time = :rand.uniform(1000)
      if sleep, do: :timer.sleep(time)
      time
    end
  end

  @tag slow: true
  test "async tasks" do
    [res_1, res_2, res_3] =
      [
        Task.async(&API.random/0),
        Task.async(&API.random/0),
        Task.async(&API.random/0)
      ]
      |> Enum.map(&Task.await/1)

    check = &(&1 > 0 && &1 < 1000)
    assert check.(res_1)
    assert check.(res_2)
    assert check.(res_3)
  end

  test "send and receive" do
    send(self(), :hello)

    result =
      receive do
        :hello -> "Hello to you too!"
        unknown -> ~s(Received unknown message: "#{unknown}")
      end

    assert "Hello to you too!" == result
  end

  test "spawn and receive" do
    parent = self()

    spawn(fn ->
      send(parent, :rand.uniform(1000))
    end)

    result =
      receive do
        random -> "Received #{random}"
      after
        500 ->
          "Something went wrong."
      end

    assert String.starts_with?(result, "Received ")
  end
end
