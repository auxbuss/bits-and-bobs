defmodule DateStuffTest do
  use ExUnit.Case

  test "date stuff" do
    assert_raise InvalidDateFormat, fn ->
      DateStuff.date_match("fred")
    end

    assert_raise InvalidDateFormat, fn ->
      DateStuff.date_match("xx/yy/zz")
    end

    assert_raise InvalidDateFormat, fn ->
      DateStuff.date_match("xx/yy/zzzz")
    end

    assert DateStuff.date_match("07/10/2017") == [7, 10, 2017]
  end
end
