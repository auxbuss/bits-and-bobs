defmodule MetaprogTest do
  use ExUnit.Case
  import ExUnit.CaptureIO
  import Metaprog

  test "one_plus_two" do
    assert 3 == one_plus_two()
  end

  test "say_hi" do
    assert capture_io(fn ->
             say_hi()
           end) == "hello world\n"
  end

  test "opts" do
    assert %{time: :am} == opts()
  end

  test "eval_quoted" do
    data = {1, 2, 3}

    ast =
      quote do
        IO.inspect(unquote(Macro.escape(data)))
      end

    assert capture_io(fn ->
             Code.eval_quoted(ast)
           end) == "{1, 2, 3}\n"

    assert capture_io(fn ->
             Code.eval_quoted(ast)
           end) == Macro.to_string(data) <> "\n"
  end

  test "say_hi_again" do
    assert "hello world" == say_hi_again("world")
  end
end
