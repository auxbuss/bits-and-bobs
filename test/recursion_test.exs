defmodule RecursionTest do
  use ExUnit.Case

  test "compute factorials" do
    assert 1         == Recursion.factorial(0)
    assert 120       == Recursion.factorial(5)
    assert 3_628_800 == Recursion.factorial(10)
  end

  test "factorial of negative" do
    assert_raise FunctionClauseError, fn ->
      Recursion.factorial(-1)
    end
  end
end
