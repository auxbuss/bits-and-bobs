defmodule CacheTest do
  use ExUnit.Case

  test "empty cache" do
    assert ConCache.get(:my_cache, :missing) == nil
    assert ConCache.delete(:my_cache, :missing) == :ok
  end

  test "puts and gets" do
    assert ConCache.size(:my_cache) == 0
    assert ConCache.put(:my_cache, :user, :fred) == :ok
    assert ConCache.insert_new(:my_cache, :user, :fredr) == {:error, :already_exists}
    assert ConCache.get(:my_cache, :user) == :fred
    assert ConCache.put(:my_cache, :user, :barney) == :ok
    assert ConCache.get(:my_cache, :user) == :barney
    assert ConCache.size(:my_cache) == 1
    assert ConCache.delete(:my_cache, :user)
    assert ConCache.size(:my_cache) == 0
  end

  test "dirty puts and gets" do
    assert ConCache.size(:my_cache) == 0
    assert ConCache.dirty_put(:my_cache, :user, :fred) == :ok
    assert ConCache.dirty_insert_new(:my_cache, :user, :fredr) == {:error, :already_exists}
    assert ConCache.get(:my_cache, :user) == :fred
    assert ConCache.dirty_put(:my_cache, :user, :barney) == :ok
    assert ConCache.get(:my_cache, :user) == :barney
    assert ConCache.size(:my_cache) == 1
    assert ConCache.dirty_delete(:my_cache, :user)
    assert ConCache.size(:my_cache) == 0
  end
end
