defmodule TcpHeaderTest do
  use ExUnit.Case

  test "tcp header generator" do
    ip = fn addr, base when base in [2, 10, 16] ->
      <<a::8, b::8, c::8, d::8>> = <<addr::32>>

      Integer.to_string(a, base) <>
        ":" <>
        Integer.to_string(b, base) <>
        ":" <> Integer.to_string(c, base) <> ":" <> Integer.to_string(d, base)
    end

    ip_header_frames =
      "45 00 00 30 44 22 40 00 80 06 00 00 C0 A8 01 10 C0 A8 01 20 45 00 00 30 44 22 40 00 80 06 00 00 C0 A8 01 24 C0 A8 01 22"
      |> String.split()
      |> Enum.reduce(<<>>, fn b, a -> a <> <<String.to_integer(b, 16)>> end)

    comp =
      for <<_ver::4, _hdr_length::4, _dcsp::6, _ecn::2, _total_length::16, _id::16, _flags::3,
            _frag_offset::13, _ttl::8, _proto::8, _hdr_csum::16, src::32,
            dst::32 <- ip_header_frames>>,
          into: %{},
          do: {ip.(src, 10), ip.(dst, 10)}

    assert comp == %{"192:168:1:16" => "192:168:1:32", "192:168:1:36" => "192:168:1:34"}
  end
end
