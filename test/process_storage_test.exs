defmodule ProcessStorageTest do
  use ExUnit.Case

  test "put and get" do
    pid = ProcessStorage.start([0])
    assert is_pid(pid)
    assert pid == Process.whereis(:storage)
    assert {:put, 1} == ProcessStorage.put(1)
    assert {:put, 2} == ProcessStorage.put(2)
    assert [0, 1, 2] == ProcessStorage.get()
    assert [0, 1, 2] == ProcessStorage.get()
  end

  test "state with Agent" do
    Agent.start_link(fn -> [0] end, name: __MODULE__)
    Agent.update(__MODULE__, &(&1 ++ [1]))
    Agent.update(__MODULE__, &(&1 ++ [2]))
    assert [0, 1, 2] == Agent.get(__MODULE__, & &1)
  end
end
