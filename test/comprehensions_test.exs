defmodule ComprehensionsTest do
  use ExUnit.Case

  test "range generator" do
    comp = for i <- 1..10, do: i * i
    assert comp == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
  end

  test "simple list generation" do
    assert Enum.to_list(1..5) == [1, 2, 3, 4, 5]
    assert Enum.into(1..5, []) == [1, 2, 3, 4, 5]
    assert Enum.map(1..5, & &1) == [1, 2, 3, 4, 5]
  end

  test "filtered range generator" do
    comp = for i <- 1..10, rem(i * i, 2) != 0, do: i * i
    assert comp == [1, 9, 25, 49, 81]
  end

  test "custom generator" do
    generator = [{:boy, %{name: "rob"}}, {:girl, %{name: "jenny"}}, {:girl, %{name: "denise"}}]
    comp = for {:girl, p} <- generator, do: p
    assert comp == [%{name: "jenny"}, %{name: "denise"}]
  end

  test "filtered custom generator" do
    generator = [{:boy, %{name: "rob"}}, {:girl, %{name: "jenny"}}, {:girl, %{name: "denise"}}]
    comp = for {:girl, p} <- generator, Regex.match?(~r/jen*/i, p.name), do: p
    assert comp == [%{name: "jenny"}]
  end

  test "filtered custom generator into a map" do
    generator = [
      {:boy, %{name: "rob"}},
      {:girl, %{name: "jenny"}},
      {:girl, %{name: "denise"}},
      {:girl, %{name: "jenna"}}
    ]

    comp =
      for {:girl, p} <- generator,
          Regex.match?(~r/jen*/i, p.name),
          into: %{},
          do: {p.name, :jname}

    assert comp == %{"jenny" => :jname, "jenna" => :jname}
  end

  test "filtered string generator" do
    gen = "The quick brown fox jumped over the moon"
    comp = for <<c <- gen>>, c != ?e && c != ?o, do: c
    assert comp == 'Th quick brwn fx jumpd vr th mn'
  end
end
