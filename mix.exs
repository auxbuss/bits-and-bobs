defmodule BitsAndBobs.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bits_and_bobs,
      version: "0.1.0",
      elixir: "~> 1.7",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      mod: {BitsAndBobs, []},
      extra_applications: [:logger, :con_cache]
    ]
  end

  defp deps do
    [
      {:con_cache, "~> 0.12.0"}
    ]
  end
end
